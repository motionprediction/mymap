import java.util.*;
import java.awt.Graphics;
import java.awt.Color;

public class TrapezoidFace {
  public Shape top;
  public Shape bottom;
  public Point leftp;
  public Point rightp;

  public TrapezoidFace upperLeft;
  public TrapezoidFace lowerLeft;
  public TrapezoidFace upperRight;
  public TrapezoidFace lowerRight;

  public Node n = null;

  public boolean merged = false;
  public boolean selected = false;

  private int index = 0;
  
  public TrapezoidFace() {
    this.top = null;
    this.bottom = null;
    this.leftp = null;
    this.rightp = null;

    this.upperLeft = null;
    this.lowerLeft = null;
    this.upperRight = null;
    this.lowerRight = null;
  }

  public TrapezoidFace(Shape top, Shape bottom, Point leftp, Point rightp) {
    this.top = top;
    this.bottom = bottom;
    this.leftp = leftp;
    this.rightp = rightp;

    this.upperLeft = null;
    this.lowerLeft = null;
    this.upperRight = null;
    this.lowerRight = null;
  }

  public TrapezoidFace(Shape top, Shape bottom, Point leftp, Point rightp, TrapezoidFace upperLeft, TrapezoidFace lowerLeft, TrapezoidFace upperRight, TrapezoidFace lowerRight) {
    this.top = top;
    this.bottom = bottom;
    this.leftp = leftp;
    this.rightp = rightp;
    this.upperLeft = upperLeft;
    this.lowerLeft = lowerLeft;
    this.upperRight = upperRight;
    this.lowerRight = lowerRight;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public Shape getTop(){
    return top;
  }
  public Shape getBottom() {
    return bottom;
  }
  public Point getLeft() {
    return leftp;
  }
  public Point getRight() {
    return rightp;
  }
  public void setTop(Shape top) {
    this.top = top;
  }
  public void setBottom(Shape bottom) {
    this.bottom = bottom;
  }
  public void setLeft(Point leftp) {
    this.leftp = leftp;
  }
  public void setRight(Point rightp) {
    this.rightp = rightp;
  }

  public void setNeighbors(TrapezoidFace upperLeft, TrapezoidFace lowerLeft, TrapezoidFace upperRight, TrapezoidFace lowerRight) {
    this.upperLeft = upperLeft;
    this.lowerLeft = lowerLeft;
    this.upperRight = upperRight;
    this.lowerRight = lowerRight;
  }

  public List<TrapezoidFace> getNeighbors() {
    List<TrapezoidFace> neighbors = new ArrayList<TrapezoidFace>();
    if(upperLeft != null) neighbors.add(upperLeft);
    if(lowerLeft != null) neighbors.add(lowerLeft);
    if(upperRight != null) neighbors.add(upperRight);
    if(lowerRight != null) neighbors.add(lowerRight);
    return neighbors;
  } 
  
  public void draw(Graphics g, int width, int height) {

    Point l = new Point(0,0);
    Point r = new Point(width,height);

    if(leftp!=null) {
      l.x = leftp.x;
      l.y = leftp.y;
    }

    if(rightp!=null) {
      r.x = rightp.x;
      r.y = rightp.y;
    }
    
    if(l.x > r.x) {
    	int tx = l.x;
    	int ty = l.y;
    	l.x = r.x;
    	l.y = r.y;
    	r.x = tx;
    	r.y = ty;    	
    }
    
    
      if(top==null) r.y = 0;
      if(bottom==null) l.y = height;
    
      if(selected) g.setColor(Color.green);
      g.drawString("" + this.index,(r.x+l.x)/2,(r.y+l.y)/2);
      g.setColor(Color.blue);


      if(top!=null) {
        g.drawLine(l.x,l.y,l.x,top.intersect(l));
        g.drawLine(r.x,r.y,r.x,top.intersect(r));
      }
      else {
        g.drawLine(l.x,l.y,l.x,0);
        g.drawLine(r.x,r.y,r.x,0);
      }
      if(bottom!=null) {
        g.drawLine(l.x,l.y,l.x,bottom.intersect(l));
        g.drawLine(r.x,r.y,r.x,bottom.intersect(r));
      }
      else {
        g.drawLine(l.x,l.y,l.x,height);
        g.drawLine(r.x,r.y,r.x,height);
 
      }

 }

  private boolean isEmpty() {
    return top==null && bottom==null && leftp==null && rightp==null;
  }

  public boolean equals(TrapezoidFace other) {
    return other==null ? isEmpty() : ( (top==null ? other.top==null : top.equals(other.top)) &&
        (bottom==null ? other.bottom==null : bottom.equals(other.bottom)) &&
        (leftp==null ? other.leftp==null : leftp.equals(other.leftp)) &&
        (rightp==null ? other.rightp==null : rightp.equals(other.rightp)) &&
        upperLeft==other.upperLeft &&
        lowerLeft==other.lowerLeft &&
        upperRight==other.upperRight &&
        lowerRight==other.lowerRight);

  }

}
